use nom::{
    bytes::complete::{tag, take},
    combinator::complete,
    multi::many0,
    number::complete::{be_u16, be_u8},
    IResult,
};
use rand::{rngs::OsRng, Rng};
use std::{
    io::{Read, Write},
    net::{IpAddr, Ipv4Addr, TcpStream},
};
use thiserror::Error;

const MAGIC_COOKIE: [u8; 4] = [33_u8, 18, 164, 66];

fn stun_header(i: &[u8]) -> IResult<&[u8], StunHeader> {
    let (i, header_type) = be_u8(i)?;
    let (i, method) = be_u8(i)?;
    let (i, body_length) = be_u16(i)?;
    let (i, _) = tag(&MAGIC_COOKIE)(i)?;
    let (i, transaction_id) = take(12_usize)(i)?;
    Ok((
        i,
        StunHeader {
            header_type,
            method,
            body_length,
            transaction_id,
        },
    ))
}

#[derive(Debug)]
struct StunHeader<'a> {
    header_type: u8,
    method: u8,
    body_length: u16,
    transaction_id: &'a [u8],
}

fn stun_body(i: &[u8]) -> IResult<&[u8], Vec<StunAttribute>> {
    let (i, attributes) = many0(complete(stun_attribute))(i)?;

    Ok((i, attributes))
}

fn stun_attribute(i: &[u8]) -> IResult<&[u8], StunAttribute> {
    let (i, attribute_type) = be_u16(i)?;
    let (i, length) = be_u16(i)?;
    let (i, value) = take(length)(i)?;
    let (i, _) = take((4 - (value.len() % 4)) % 4)(i)?;

    Ok((
        i,
        StunAttribute {
            attribute_type,
            value,
        },
    ))
}

#[derive(Debug, PartialEq)]
struct StunAttribute<'a> {
    attribute_type: u16,
    value: &'a [u8],
}

fn stun_xor_mapped_ipv4_address_attribute(i: &[u8]) -> IResult<&[u8], StunXorMappedAttribute> {
    let (i, _) = tag([0_u8, 1])(i)?;
    let (i, _port) = be_u16(i)?;
    let (i, mapped_addr) = take(4_usize)(i)?;

    Ok((i, StunXorMappedAttribute::Ipv4(mapped_addr)))
}

enum StunXorMappedAttribute<'a> {
    Ipv4(&'a [u8]),
}

pub struct Client {
    stream: TcpStream,
}

impl Client {
    pub fn talk_to_server_via(stream: TcpStream) -> Self {
        Self { stream }
    }

    pub fn get_ip_addr(&mut self) -> Result<IpAddr, Error> {
        let mut transaction_id = [0_u8; 12];
        OsRng.fill(&mut transaction_id);

        self.stream.write_all(&[0_u8])?; // type: binding_request
        self.stream.write_all(&[1_u8])?; // method: binding
        self.stream.write_all(&0_u16.to_be_bytes())?; // message length
        self.stream.write_all(&MAGIC_COOKIE)?;
        self.stream.write_all(&transaction_id)?;

        let mut response_header = [0_u8; 20];
        self.stream.read_exact(&mut response_header)?;
        let header = stun_header(&response_header)?.1;

        let mut response_body = vec![0_u8; header.body_length.into()];
        self.stream.read_exact(&mut response_body)?;
        let attributes: &[StunAttribute] = &stun_body(&response_body)?.1;
        let xor_mapped_addr = attributes
            .iter()
            .find(|attribute| attribute.attribute_type == 0x00_20_u16)
            .ok_or(Error::XorMappedAddressNotFound)?;

        match stun_xor_mapped_ipv4_address_attribute(&xor_mapped_addr.value)?.1 {
            StunXorMappedAttribute::Ipv4(bytes) => Ok(IpAddr::V4(Ipv4Addr::new(
                bytes[0] ^ MAGIC_COOKIE[0],
                bytes[1] ^ MAGIC_COOKIE[1],
                bytes[2] ^ MAGIC_COOKIE[2],
                bytes[3] ^ MAGIC_COOKIE[3],
            ))),
        }
    }
}

#[derive(Error, Debug)]
pub enum Error {
    #[error(transparent)]
    Io(#[from] std::io::Error),
    #[error("Response does not contain IPv4 address")]
    XorMappedAddressNotFound,
    #[error("Protocol parsing error: not enough data")]
    NotEnoughData,
    #[error("Protocol parsing error: invalid response")]
    InvalidResponse,
}

impl From<nom::Err<(&[u8], nom::error::ErrorKind)>> for Error {
    fn from(e: nom::Err<(&[u8], nom::error::ErrorKind)>) -> Self {
        match e {
            nom::Err::Incomplete(_) => Error::NotEnoughData,
            nom::Err::Error(_) | nom::Err::Failure(_) => Error::InvalidResponse,
        }
    }
}
