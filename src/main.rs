mod cloudflare;
mod stun;

use anyhow::Context;
use cloudflare::Cloudflare;
use docopt::Docopt;
use log::{error, info};
use pretty_env_logger;
use serde_derive::Deserialize;
use std::{
    net::{IpAddr::V4, TcpStream},
    process,
};

const USAGE: &str = r#"
Updates DNS record on Cloudflare to match current WAN IPv4

Usage:
  cloudflare-ddns [options] <zone-id> <record-name> <auth-file>
  cloudflare-ddns -h | --help

Options:
  -h, --help            Show this screen.
  --stun-server=<addr>  STUN server socket address [default: stun.stunprotocol.org:3478].
  --force               Updates DNS record regardless of its current value.
  --dry-run             Don't actually update DNS record. Instead only show what would have happened.

Parameters:
  <zone-id>     It can be found in Overview's page API section. It's a 16 hexadecimal symbols string.
  <record-name> A targeted DNS record name of type 'A'. E.g. 'subdomain.example.org'.
                DNS record must already exist.
  <auth-file>   Path to a JSON file which contains API credentials which can be found in My Profile,
                under API Keys section.
                File must be a JSON object { "auth_email": "...", "auth_key": "..." }
"#;

#[derive(Debug, Deserialize)]
struct Args {
    arg_zone_id: String,
    arg_record_name: String,
    arg_auth_file: String,
    flag_stun_server: String,
    flag_force: bool,
    flag_dry_run: bool,
}

fn main() -> anyhow::Result<()> {
    pretty_env_logger::init_timed();

    let args: Args = Docopt::new(USAGE)
        .and_then(|d| d.deserialize())
        .unwrap_or_else(|e| e.exit());

    let cloudflare = Cloudflare::from_file_auth(&args.arg_auth_file)
        .context("Could not create Cloudflare client.")?;
    let stun_tcp_stream = TcpStream::connect(&args.flag_stun_server).context(format!(
        "Could not connect to STUN server at `{}`.",
        &args.flag_stun_server
    ))?;
    let ip_address = stun::Client::talk_to_server_via(stun_tcp_stream).get_ip_addr()?;

    if let V4(ip_v4_address) = ip_address {
        const RECORD_TYPE: &str = "A";

        let mut record = cloudflare
            .get_dns_record(&args.arg_zone_id, &args.arg_record_name, RECORD_TYPE)
            .context("Could not get DNS record.")?;
        let ip_v4_address = ip_v4_address.to_string();
        if args.flag_force || ip_v4_address.ne(&record.content) {
            if args.flag_dry_run {
                info!("Would update DNS record to IP {}", &ip_v4_address);
            } else {
                record.content = ip_v4_address.clone();
                cloudflare
                    .update_dns_record_value(&record)
                    .context("Could not update DNS record.")?;
                println!(
                    "{} {} {}",
                    &record.name, &record.record_type, &ip_v4_address
                );
                info!("DNS record updated to IP {}", &ip_v4_address);
            }
        } else {
            info!(
                "DNS record update skipped. Record is already set to {}",
                &ip_v4_address
            );
        }
    } else {
        error!("IP address was not V4");
        process::exit(1);
    };
    Ok(())
}
