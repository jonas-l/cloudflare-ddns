use minreq;
use serde_derive::{Deserialize, Serialize};
use std::fs;
use thiserror::Error;

pub struct Cloudflare {
    auth_email: String,
    auth_key: String,
}

#[derive(Debug, Deserialize)]
struct Auth {
    auth_email: String,
    auth_key: String,
}

#[derive(Debug, Deserialize)]
struct ListDnsRecords {
    success: bool,
    errors: Vec<String>,
    messages: Vec<String>,
    result: Vec<DnsRecord>,
}

#[derive(Debug, Deserialize)]
struct UpdateDnsRecord {
    success: bool,
    errors: Vec<String>,
    messages: Vec<String>,
    result: DnsRecord,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct DnsRecord {
    pub id: String,
    pub zone_id: String,
    pub name: String,
    #[serde(rename = "type")]
    pub record_type: String,
    pub content: String,
}

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not read Cloudflare config at `{file_name}`")]
    ReadingConfig {
        file_name: String,
        source: std::io::Error,
    },
    #[error(transparent)]
    ParsingConfig(#[from] serde_json::error::Error),
    #[error(transparent)]
    ApiCall(#[from] std::io::Error),
    #[error("Could not get DNS record:\n{0:?}")]
    DnsRecordGet(Vec<String>),
    #[error("Could not update DNS record:\n{0:?}")]
    DnsRecordUpdate(Vec<String>),
    #[error("Missing {zone_id} {record_name} {record_type}")]
    MissingDnsRecord {
        zone_id: String,
        record_name: String,
        record_type: String,
    },
}

const CLOUDFLARE_API_URL: &str = "https://api.cloudflare.com/client/v4";

impl Cloudflare {
    pub fn from_file_auth(auth_file: &str) -> Result<Self, Error> {
        let auth_file_content =
            fs::read_to_string(auth_file).map_err(|e| Error::ReadingConfig {
                file_name: auth_file.to_owned(),
                source: e,
            })?;
        let auth: Auth = serde_json::from_str(&auth_file_content)?;

        Ok(Self {
            auth_email: auth.auth_email,
            auth_key: auth.auth_key,
        })
    }

    pub fn get_dns_record(
        &self,
        zone_id: &str,
        record_name: &str,
        record_type: &str,
    ) -> Result<DnsRecord, Error> {
        let dns_records_url = format!(
            "{api_url}/zones/{zone_id}/dns_records?type={record_type}&name={record_name}",
            api_url = CLOUDFLARE_API_URL,
            zone_id = zone_id,
            record_type = record_type,
            record_name = record_name,
        );

        let records_response: ListDnsRecords = minreq::get(dns_records_url)
            .with_header("X-Auth-Email", self.auth_email.to_string())
            .with_header("X-Auth-Key", self.auth_key.to_string())
            .send()?
            .json()?;

        match &records_response.success {
            true => {
                records_response
                    .result
                    .into_iter()
                    .next()
                    .ok_or_else(|| Error::MissingDnsRecord {
                        zone_id: zone_id.to_owned(),
                        record_name: record_name.to_owned(),
                        record_type: record_type.to_owned(),
                    })
            }
            false => Err(Error::DnsRecordGet(records_response.errors)),
        }
    }

    pub fn update_dns_record_value(&self, record: &DnsRecord) -> Result<(), Error> {
        let dns_record_url = format!(
            "{api_url}/zones/{zone_id}/dns_records/{record_id}",
            api_url = CLOUDFLARE_API_URL,
            zone_id = record.zone_id,
            record_id = record.id
        );

        let record_response: UpdateDnsRecord = serde_json::from_str(
            &minreq::put(dns_record_url)
                .with_header("X-Auth-Email", self.auth_email.to_string())
                .with_header("X-Auth-Key", self.auth_key.to_string())
                .with_header("Content-Type", "application/json")
                .with_json(&record)?
                .send()?
                .body,
        )?;

        if record_response.success {
            Ok(())
        } else {
            Err(Error::DnsRecordUpdate(record_response.errors))
        }
    }
}
