Cross-compilation
=================
Project uses [`rustls`][1] instead of OpenSSL to allow easy cross-compilation.


For musl Linux on MacOS host
----------------------------
First of all, you need compiler as well as toolchain support for target platform:

```shell
brew install FiloSottile/musl-cross/musl-cross
rustup target add x86_64-unknown-linux-musl
```

then, configure cargo by updating `.cargo/config` file in project or user home directory:

```
[target.x86_64-unknown-linux-musl]
linker = "x86_64-linux-musl-cc"
```

and finally, build target using cargo:

```shell
CC_x86_64_unknown_linux_musl=x86_64-linux-musl-gcc cargo build --release --target x86_64-unknown-linux-musl
```

Note environment variable `CC_x86_64_unknown_linux_musl` which is required for `ring` dependency to compile.


For Windows on MacOS host
-------------------------
First of all, you need compiler as well as toolchain support for target platform:

```shell
brew install mingw-w64
rustup target add x86_64-pc-windows-gnu
```

then, configure cargo by updating `.cargo/config` file in project or user home directory:

```
[target.x86_64-pc-windows-gnu]
linker = "x86_64-w64-mingw32-gcc"
ar = "x86_64-w64-mingw32-gcc-ar"
```

and finally, build target using cargo:

```shell
cargo build --release --target x86_64-pc-windows-gnu
```


[1]: https://crates.io/crates/rustls
