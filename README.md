Cloudflare DDNS 
===============
Determines current IP address using [STUN][1] protocol and updates DNS record via [Cloudflare API][2].


Getting started
---------------
Create a file `cloudflare-auth.json` next to the `cloudflare-ddns` executable with the following content:

```json
{
  "auth_email": "<Cloudflare account email address>",
  "auth_key": "<Global API Key>"
}
```

Here, email address is the one you use to login to Cloudflare dashboard. API Key can be found at _[Profile page][3]_ under _API Keys_ > _Global API Key_ > _View_.

Make sure you have DNS record available: from Cloudflare dashboard home screen select appropriate domain (e.g. _example.org_), and open DNS tab. Make sure there's 'A' record you want to update (e.g. _ddns_).

Finally, you need to obtain _Zone ID_ which can be found in domains overview page under _API_ > _Zone ID_ (e.g. _44a25dbc81c9c0065a5836d91857b622_).

You are now prepared to perform DNS record update:

```shell
cloudflare-ddns 44a25dbc81c9c0065a5836d91857b622 ddns.example.org cloudflare-auth.json
```

Here first argument is _Zone ID_ followed by full record name and finally a path to authorization details' file you created at the beginning.

If everything went well you should see output similar to

```
ddns.example.org A 1.2.3.4
```


Troubleshooting
---------------
By default logging is set to `error` level, but you can override settings by providing environment variable `RUST_LOG`:

```shell
RUST_LOG=info cloudflare-ddns 44a25dbc81c9c0065a5836d91857b622 ddns.example.org cloudflare-auth.json
```

You can consult documentation of [`env_logger` crate][4] on how to specify levels for different loggers.


Releases
--------
Release is trivial using Cargo:

```shell
cargo build --release
``` 

Also there's a guide how to [cross-compile on MacOS host](docs/cross-compile.md) for other platforms.


[1]: https://tools.ietf.org/html/rfc5389
[2]: https://api.cloudflare.com/
[3]: https://dash.cloudflare.com/profile
[4]: https://crates.io/crates/env_logger
